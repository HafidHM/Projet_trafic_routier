/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ussop
 */
import java.awt.Color;
import java.awt.Graphics;
import java.util.Vector;
import javax.swing.JPanel;

public class Panneau extends JPanel {
    Vector<Feu> f;
    Vector<Vehicules> v;
    
public void paintComponent(Graphics g){
      
       g.setColor(Color.white);
       g.fillRect(0, 0, this.getWidth(), this.getHeight());
      
       /* route  horizontale */
       g.setColor(Color.black);
       g.fillRect(0, 400, 1000, 200);
      
       g.setColor(Color.white);
       g.fillRect(10,490,60,20);
      
       g.setColor(Color.white);
       g.fillRect(90,490,60,20);
       
       g.setColor(Color.white);
       g.fillRect(170,490,60,20);
       
       g.setColor(Color.white);
       g.fillRect(250,490,60,20);
       
       g.setColor(Color.white);
       g.fillRect(330,490,60,20);
       
       g.setColor(Color.white);
       g.fillRect(610,490,60,20);
       
       g.setColor(Color.white);
       g.fillRect(690,490,60,20);
       
       g.setColor(Color.white);
       g.fillRect(770,490,60,20);
       
       g.setColor(Color.white);
       g.fillRect(850,490,60,20);
       
       g.setColor(Color.white);
       g.fillRect(930,490,60,20);
       
        /*route  verticale*/
       g.setColor(Color.black);
       g.fillRect(400,0,200,1000);
      
       g.setColor(Color.white);
       g.fillRect(490,10,20,60);
      
       g.setColor(Color.white);
       g.fillRect(490,90,20,60);
       
       g.setColor(Color.white);
       g.fillRect(490,170,20,60);
       
       g.setColor(Color.white);
       g.fillRect(490,250,20,60);
       
       g.setColor(Color.white);
       g.fillRect(490,330,20,60);
       
       g.setColor(Color.white);
       g.fillRect(490,610,20,60);
       
       g.setColor(Color.white);
       g.fillRect(490,690,20,60);
       
       g.setColor(Color.white);
       g.fillRect(490,770,20,60);
       
       g.setColor(Color.white);
       g.fillRect(490,850,20,60);
       
       g.setColor(Color.white);
       g.fillRect(490,930,20,60);
      
       /* passage pieton gauche */
       g.setColor(Color.white);
       g.fillRect(330,410,60,20);
      
       g.setColor(Color.white);
       g.fillRect(330,450,60,20);
      
       g.setColor(Color.white);
       g.fillRect(330,530,60,20);
      
       g.setColor(Color.white);
       g.fillRect(330,570,60,20);
      
       /* passage pieton droite */
       g.setColor(Color.white);
       g.fillRect(610,570,60,20);
      
       g.setColor(Color.white);
       g.fillRect(610,530,60,20);
      
       g.setColor(Color.white);
       g.fillRect(610,450,60,20);
      
       g.setColor(Color.white);
       g.fillRect(610,410,60,20);
      
       /* passage pieton haut */
       g.setColor(Color.white);
       g.fillRect(570,330,20,60);
      
       g.setColor(Color.white);
       g.fillRect(530,330,20,60);
      
       g.setColor(Color.white);
       g.fillRect(450,330,20,60);
      
       g.setColor(Color.white);
       g.fillRect(410,330,20,60);
      
       /*FIN PASSAGE*/
      
       /* passage pieton bas */
       g.setColor(Color.white);
       g.fillRect(570,610,20 ,60);
      
       g.setColor(Color.white);
       g.fillRect(530,610,20,60);
      
       g.setColor(Color.white);
       g.fillRect(450,610,20,60);
      
       g.setColor(Color.white);
       g.fillRect(410,610,20,60);
      
       /*FIN PASSAGE*/
       if (f != null){
            for(int i=0;i<f.size();i++)
            {
                g.setColor(f.get(i).getClr());
                g.fillOval(f.get(i).getX(),f.get(i).getY(),30,30);
            }
       }
       
       if (v != null){
           for(int i=0;i<v.size();i++)
            {
                g.setColor(v.get(i).getClr());
                g.fillRect(v.get(i).getX(),v.get(i).getY(),30,30);
            }
       }
      
       }
      

       public void setFeu(Vector<Feu> f1) {
             f=f1;
       }
       public void setVehicule(Vector<Vehicules> v1) {
             v=v1;
       }
       }