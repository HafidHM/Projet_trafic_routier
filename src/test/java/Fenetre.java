
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author ussop
 */
import java.awt.Color;
import java.util.Vector;
import javax.swing.JFrame;

public class Fenetre extends JFrame{
    private Panneau pan = new Panneau();
    private Route r1=new Route(1,"national");

public Fenetre(){
    this.setTitle("Animation");
    this.setSize(1000,1000);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.setLocationRelativeTo(null);
    this.setContentPane(pan);
    this.setVisible(true);

go();
}
private void go(){
    Feu f1=new Feu(1,Color.green,10000,312,605);
    Feu f2=new Feu(2,Color.red,10000,365,312);
    Feu f3=new Feu(3,Color.green,10000,656,365);
    Feu f4=new Feu(4,Color.red,10000,605,656);

    Vehicules v1=new Vehicules(1,Color.red,8,0,540);
    Vehicules v2=new Vehicules(2,Color.red,-8,1000,430);
    Vehicules v3=new Vehicules(3,Color.red,8,48,540);
    
    Vehicules v4=new Vehicules(4,Color.blue,-8,540,1000);

    Vehicules v5=new Vehicules(5,Color.blue,8,430,80);
    Vehicules v6=new Vehicules(6,Color.blue,-8,540,952);
    
    Vehicules v7=new Vehicules(7,Color.blue,8,430,0);
    Vehicules v8=new Vehicules(8,Color.blue,-8,540,904);
    
    Vehicules v9=new Vehicules(9,Color.blue,8,430,40);
    Vehicules v10=new Vehicules(10,Color.blue,-8,540,856);

    r1.addFeu(f1);
    r1.addFeu(f2);
    r1.addFeu(f3);
    r1.addFeu(f4);
    r1.addVehicule(v1);
    r1.addVehicule(v2);
    r1.addVehicule(v3);
    r1.addVehicule(v4);
    r1.addVehicule(v5);
    r1.addVehicule(v6);
    r1.addVehicule(v7);
    r1.addVehicule(v8);
    r1.addVehicule(v9);
    r1.addVehicule(v10);
    
    Vector<Feu> f=r1.getF();
    Vector<Vehicules> v=r1.getVehicules();
    Vehicules_IA vehicule_IA = new Vehicules_IA();
    
    for(int i=0;i<f.size();i++)
    {
       f.get(i).start();
    }
   
    for(int i=0;i<v.size();i++)
    {
       v.get(i).start();
    }

while(true)
{

       //System.out.println(v1.getX());
       //System.out.println(f1.getClr());
       try {
             Thread.sleep(60);
       } catch (InterruptedException e) {
             e.printStackTrace();
       }
        pan.setFeu(r1.getF());
        pan.setVehicule(r1.getVehicules());
        
        // Cette fonction prend la liste de tout les véhicules (Route 1 + Route 2)
        vehicule_IA.run(v, f);

        // Voiture route horizontale
        if(v1.getX()>pan.getWidth())
            v1.setX(0);
        if(v2.getX()<=0)
            v2.setX(pan.getWidth());
        if(v3.getX()>pan.getWidth())
            v3.setX(0);
        
        // Voiture route verticale
        if(v4.getY()<=0)
            v4.setY(pan.getHeight());
        
        if(v5.getY()>pan.getHeight())
            v5.setY(0);
        
        if(v6.getY()<=0)
            v6.setY(pan.getHeight());
        
        if(v7.getY()>pan.getHeight())
            v7.setY(0);
        
        if(v8.getY()<=0)
            v8.setY(pan.getHeight());
        
        if(v9.getY()>pan.getHeight())
            v9.setY(0);
        
        if(v10.getY()<=0)
            v10.setY(pan.getHeight());

        pan.repaint();
        }
}

public static void main(String[] args) {
       Fenetre f=new Fenetre();
}
}

