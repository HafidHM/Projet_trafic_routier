
import java.awt.Color;
import java.util.Vector;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author ussop
 */
public class Vehicules_IA {
    
    public Vehicules_IA(){
        //this.f = new Vector<>(feux);
        //this.v = new Vector<>(vehicules);
    }
    
    public void run(Vector<Vehicules> v, Vector<Feu> f){
        // Parcours des véhicules
        for (Vehicules vehicule : v) {
            vehicule.setCalcul(true);
            vehicule.setRoule(true);
            if (vehicule.getNum()==1 || vehicule.getNum()==3){
                //System.out.println("Véhicule numéro "+ vehicule.getNum() + " is roule initialisation " +vehicule.isRoule());
            }
            
            for (Vehicules vehiculeDevant : v) {
            
                if (vehicule.getNum() == 1 || vehicule.getNum() == 3){
                    // Si un véhicule est proche du premier feu 
                    // Deuxième condition : Si la voiture est dans bonne partie de la route (500 = milieu de la route).
                    // Si le feu est rouge
                    if ((Math.abs(vehicule.getX() - f.get(0).getX()) <= 16 && vehicule.getY() > 500 && f.get(0).getClr() == Color.red)){
                        vehicule.setRoule(false);   
                    } else if ((vehiculeDevant.getX() - vehicule.getX() <= 70 && vehiculeDevant.getX() > vehicule.getX() && (vehiculeDevant.getY() < vehicule.getY()+60 && vehiculeDevant.getY() >= vehicule.getY() - 15))){
                        vehicule.setRoule(false);
                    }
                }
                
                if (vehicule.getNum() == 5 || vehicule.getNum() == 7 || vehicule.getNum() == 9){
                    // Si un véhicule est proche du deuxième feu 
                    // Deuxième condition : Si la voiture est dans bonne partie de la route (500 = milieu de la route).
                    // Si le feu est rouge
                    if ((Math.abs(vehicule.getY() - f.get(1).getY()) <= 16 && vehicule.getX() < 500 && f.get(1).getClr() == Color.red)){
                        vehicule.setRoule(false);   
                    } else if ((vehiculeDevant.getY() - vehicule.getY() <= 70 && vehiculeDevant.getY() > vehicule.getY() && (vehiculeDevant.getX() < vehicule.getX()+45 && vehiculeDevant.getX() > vehicule.getX() - 60))){
                        vehicule.setRoule(false);
                    }//&& vehiculeDevant.getX() > vehicule.getX() - 30
                }
                
                if (vehicule.getNum() == 2){
                    // Si un véhicule est proche du troisième feu 
                    // Deuxième condition : Si la voiture est dans bonne partie de la route (500 = milieu de la route).
                    // Si le feu est rouge
                    if ((Math.abs(vehicule.getX() - f.get(2).getX()) <= 16 && vehicule.getY() < 500 && f.get(2).getClr() == Color.red)){
                        vehicule.setRoule(false);   
                    } else if ((vehicule.getX() - vehiculeDevant.getX() <= 70 && vehicule.getX() > vehiculeDevant.getX() && (vehiculeDevant.getY() < vehicule.getY()+45 && vehiculeDevant.getY() > vehicule.getY() - 60))){
                        vehicule.setRoule(false);
                    }
                }
                
                if (vehicule.getNum() == 4 || vehicule.getNum() == 6 || vehicule.getNum() == 8 || vehicule.getNum() == 10){
                    // Si un véhicule est proche du deuxième feu 
                    // Deuxième condition : Si la voiture est dans bonne partie de la route (500 = milieu de la route).
                    // Si le feu est rouge
                    if ((Math.abs(vehicule.getY() - f.get(3).getY()) <= 16 && vehicule.getX() > 500 && f.get(3).getClr() == Color.red)){
                        vehicule.setRoule(false);   
                    } else if ((vehicule.getY() - vehiculeDevant.getY() <= 70 && vehicule.getY() > vehiculeDevant.getY() && (vehiculeDevant.getX() < vehicule.getX()+60 && vehiculeDevant.getX() >= vehicule.getX() - 15))){
                        vehicule.setRoule(false);
                    }
                }
            }
            if (vehicule.getNum() == 1 || vehicule.getNum() == 3){
                //System.out.println("Vehicule numéro " + vehicule.getNum() + " is roule " + vehicule.isRoule());
            }
            vehicule.setCalcul(false);
        }
    }
}
