/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ussop
 */
import java.awt.*;

public class Feu extends Thread {

       private int num;
       private Color clr;
       private int x,y,duree;
      
       public Feu(int num, Color clr, int duree, int x, int y) {
             this.num = num;
             this.clr = clr;
             this.x = x;
             this.y = y;
             this.duree = duree;
       }
      
       public int getNum() {
             return num;
       }
      
       public void setId(int num) {
             this.num = num;
       }
      
       public Color getClr() {
             return clr;
       }
      
       public void setClr(Color clr) {
             this.clr = clr;
       }
      
       public int getX() {
             return x;
       }
      
       public void setX(int x) {
             this.x = x;
       }
      
       public int getY() {
             return y;
       }
      
       public void setY(int y) {
             this.y = y;
       }
       
       public int getDuree() {
             return duree;
       }
      
       public void setDuree(int duree) {
             this.duree = duree;
       }
      
      
       public void run()
       {
             while(true)
             {

            
             try {
                    sleep(1000);
             } catch (InterruptedException e) {
                    e.printStackTrace();
             }
             if(clr==Color.red){
            
                    clr=Color.green;
             }
             else
                    clr=Color.red;
             try {
                    sleep(duree);
             } catch (InterruptedException e) {
                    e.printStackTrace();
             }
             if(clr==Color.red)
                    clr=Color.green;
             else
                    clr=Color.red;
            
             }
       }
}
