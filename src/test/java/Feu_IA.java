
import java.util.Vector;
import java.awt.*;

import javax.swing.text.AttributeSet.ColorAttribute;

public class Feu_IA {
    public Feu_IA() {
    }

    public void run(Vector<Vehicules> v, Vector<Feu> f, Route R/* , Route R2 */) {
        // Tu dois coder ici la logique des feux
        // Pour parcourir tous les véhicules
        // Vector <Vehicules> VR1 = R1.getVehicules();
        // Vector <Vehicules> VR2 = R2.getVehicules();
        // Vector<Feu> F1 = R1.getF() ;
        /// Vector<Feu> F2 = R2.getF() ;
        //Vector<Feu> F = R.getF();

        for (Vehicules vv : v) {

            // Ensuite pour savoir si le véhicule est arrété
            if ((/* VR1.contains(vv) */ vv.getNum() < 4) && (vv.isRoule())) {
                for (Feu FF : f) {
                    /* FF.setClr(Color.red); */
                    if ((FF.getNum() == 1) || (FF.getNum() == 3))
                        FF.setClr(Color.green);
                    else
                        FF.setClr(Color.red);

                }
            }

            else if ((/* VR1.contains(vv) */ vv.getNum() > 3) && (vv.isRoule())) {
                for (Feu FF : f) {
                    if ((FF.getNum() == 2) || (FF.getNum() == 4))
                        FF.setClr(Color.green);
                    else
                        FF.setClr(Color.red);

                }

            }
        }
    }
}

/*
 * for (Feu FF:F1){ FF.setClr(Color.green); }
 * 
 * 
 * 
 * }
 * 
 * 
 * else if ( (VR2.contains(vv))&& (vv.isRoule())) { for (Feu FF:F1){
 * FF.setClr(Color.red); } for (Feu FF:F2){ FF.setClr(Color.green); }
 * 
 * 
 * 
 * }
 * 
 * 
 * 
 * } // vehicule.isRoule() => doit retourner false
 * 
 * // Dispostion des feux //Voie gauche : feu numéro 1 f(0) //Voie du haut: feu
 * numéro 2 f(1) //Voie de droite: feu numéro 3 f(2) //Voie du bas: feu numéro 4
 * f(3) }
 */
